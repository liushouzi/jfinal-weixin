/*
 * Copyright (c) 2011-2021, L.cm (596392912@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.weixin.sdk.api;

import com.jfinal.json.JsonManager;
import com.jfinal.weixin.sdk.utils.HttpUtils;
import com.jfinal.weixin.sdk.utils.JsonUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * 发布接口
 * <p>
 * 文档地址：https://developers.weixin.qq.com/doc/offiaccount/Publish/Publish.html
 *
 * @author L.cm
 */
public class FreePublishApiTest {

    @Before
    public void setUp() {
        AccessTokenApiTest.init();
        JsonManager.me().setDefaultJsonFactory(new com.jfinal.json.MixedJsonFactory());
    }

    @Test
    public void get() {
        ApiResult apiResult = FreePublishApi.get("123");
        System.out.println(apiResult);
    }

}
